const models = require("../bot/models/index");
const ServerConfig = models.ServerConfig;
const DatabaseError = require("sequelize").DatabaseError;

const DATABASEERRORMESSAGE = `Um erro ocorreu no nosso banco de dados, isso é provavelmente culpa do programador`;
const GENERALERROR = `Ocorreu um erro inesperado, contacte o programador`;
const CHANNELERROR = `Um erro ocorreu, veja se o bot tem acesso ao canal ou se o canal existe, \
caso haja duvidas fale com o dono do bot`;
const CHANNELTIP = `recomendo que configure um canal para o bot usando d!config botchannel #canal`;

const updateBotChannel = async (channel, serverConfig) => {
  try {
    if (!serverConfig) {
      serverConfig = await ServerConfig.create({ guild_id: channel.guild.id });
    }
    serverConfig.bot_channel = channel.id;
    return await serverConfig.save();
  } catch (e) {
    console.error(e);
    if (e instanceof DatabaseError) {
      channel.send(DATABASEERRORMESSAGE);
    } else {
      channel.send(GENERALERROR);
    }
    return false;
  }
};

const updateTriviaChannel = async (channel, serverConfig) => {
  try {
    if (!serverConfig) {
      serverConfig = await ServerConfig.create({ guild_id: channel.guild.id });
    }
    serverConfig.trivia_channel = channel.id;
    return await serverConfig.save();
  } catch (e) {
    console.error(e);
    if (e instanceof DatabaseError) {
      channel.send(DATABASEERRORMESSAGE);
    } else {
      channel.send(GENERALERROR);
    }
    return false;
  }
};

exports.run = async (client, message, args) => {
  if (message.guild) {
    try {
      let channel = message.mentions.channels.first();

      if (
        !channel ||
        !channel.permissionsFor(client.user).has(["VIEW_CHANNEL"])
      ) {
        throw new Error();
      }

      let serverConfig = await ServerConfig.findOne({
        where: {
          guild_id: channel.guild.id
        }
      });

      if (args[0].toLowerCase() == "botchannel") {
        serverConfig = await updateBotChannel(channel, serverConfig);
        if (serverConfig) {
          message.reply(`configurado, agora o canal de bot é: ${channel}`);
        }
      } else if (args[0].toLowerCase() == "triviachannel") {
        serverConfig = await updateTriviaChannel(channel, serverConfig);
        if (serverConfig) {
          message.reply(`configurado, agora o canal de trivia é: ${channel}`);
        }
      }

      if (serverConfig && !serverConfig.bot_channel) {
        message.reply(CHANNELTIP);
      }
    } catch (e) {
      console.error(e);
      message.channel.send(CHANNELERROR);
    }
  }
};
