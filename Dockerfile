FROM node:latest
MAINTAINER Fabricio Bezerra <demonstro003@gmail.com>

RUN apt-get update -qq && apt-get install -y --force-yes build-essential vim postgresql-client
RUN export LANG=en_US.UTF-8

RUN mkdir -p /bot
WORKDIR /bot
ADD . /bot
RUN npm install forever
RUN npm install
