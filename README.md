# Discord Trivia Bot

This is a project for a bot on discord, the objective of the bot is to create,
administrate and help on the making of trivia games.

## What is a trivia game (by my standards)

A trivia game is basically a game of question and answers, where there is an 
asker (the guy who is going to ask the questions) and the players (the people
who are going to answer the questions)

### Cool, what are the steps to a trivia game to be cool?

The asker needs to make questions, the first person to answer correctly wins the
round and the asker makes another question (the number of question can be 
decided, but it can go on indefinitely)

### If the asker can do it, why the need for a bot?

The bot is used to help the asker, it will keep track of who awsered first and 
how many points the user have, it can be improved to accept more modes of trivia
game

## How to run the bot

You can run the bot in two ways:

- Using Docker (recommended)
- Running on your own PC (not recommended and not tested)

### Same steps for both

#### What you will need

- An text editor
- An terminal that can run commands

#### Steps

1. Make a copy of the file `config.json.sample` to `config.json` and put your id
the bot token, your id and the prefix that the bot will use on the server.
2. Make a copy of the file `bot/config.json.sample` to `bot/config.json` and
put your database information there, if you are using docker the configuration
there is already right

### Using Docker

That is the easy way, please use it

#### What you will need

- Docker
- Docker-compose

#### Steps

1. Make a copy of the file `.env.docker.sample` to `.env.docker`, it's already
alright, if you want to change anything don't forget to change the other files
too
2. Run `docker-compose build`
3. Run `docker-compose run bot bash`
4. Now you are inside the bot container, see the "After same steps" to continue

PS: After the "After same steeps" you can run `docker-compose up` to make the
make bot run without entering in the container, read more 
[here](https://docs.docker.com/compose/reference/run/)

PS 2: If you want to exit the docker container just run `exit`

### Running on your own PC

Since its not recommended I will not write it yet (I don't even know how to do
it), but basically you want to have the newest node and postgres 9.6 installed

### After same steps

1. Run `npm install` to install the dependencies
2. Run `npx sequelize db:create && npx sequelize db:drop && 
npx sequelize db:migrate`
3. Run `npm run startdev` to see colors on terminal or `npm start` if you don't
care about colors
4. Your bot is running, `CTRL + C` will stop the bot
5. Success


## Contributing to the project

1. Fork the project
2. Commit your changes on a new branch
3. Open a pull request
4. :)

## License

Copyright (c) 2018 Fabricio Bezerra

It's a MIT Licence, just don't remove my name from the thing and you are good to
go!