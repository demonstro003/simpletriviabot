'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ServerConfigs', {
      trivia_running: {
        type: Sequelize.BOOLEAN
      },
      trivia_channel: {
        type: Sequelize.STRING
      },
      bot_channel: {
        type: Sequelize.STRING
      },
      guild_id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ServerConfigs');
  }
};
