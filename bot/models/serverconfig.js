'use strict';
module.exports = (sequelize, DataTypes) => {
  const ServerConfig = sequelize.define('ServerConfig', {
    trivia_running: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    trivia_channel: DataTypes.STRING,
    bot_channel: {
      type: DataTypes.STRING
    },
    guild_id: {
      type: DataTypes.STRING,
      primaryKey: true
    }
  }, {});
  ServerConfig.associate = function(models) {
    // associations can be defined here
  };
  return ServerConfig;
};
